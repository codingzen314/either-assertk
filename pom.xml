<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">

    <modelVersion>4.0.0</modelVersion>

    <groupId>tech.codingzen</groupId>
    <artifactId>either-assertk</artifactId>
    <version>1.0.0</version>
    <packaging>jar</packaging>

    <name>${project.groupId}:${project.artifactId}</name>
    <description>library containing an implementation of assertk assertions for Either</description>
    <url>https://bitbucket.org/codingzen314/either</url>

    <licenses>
        <license>
            <name>The MIT License</name>
            <url>http://www.opensource.org/licenses/MIT</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <name>devlambdaj</name>
            <email>codingzen314@gmail.com</email>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git://codingzen314@bitbucket.org/codingzen314/either-assertk.git</connection>
        <developerConnection>scm:git:ssh://codingzen314@bitbucket.org/codingzen314/either-assertk.git</developerConnection>
        <url>http://codingzen314@bitbucket.org/codingzen314/either-assertk.git</url>
    </scm>

    <pluginRepositories>
        <pluginRepository>
            <id>jcenter</id>
            <name>JCenter</name>
            <url>https://jcenter.bintray.com/</url>
        </pluginRepository>
        <pluginRepository>
            <id>central</id>
            <name>central</name>
            <url>https://repo1.maven.org/maven2/</url>
        </pluginRepository>
    </pluginRepositories>

    <properties>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <kotlin.version>1.4.32</kotlin.version>
        <kotlin.code.style>official</kotlin.code.style>
        <junit.version>4.12</junit.version>
        <kotlin.compiler.jvmTarget>1.8</kotlin.compiler.jvmTarget>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-stdlib-jdk8</artifactId>
            <version>${kotlin.version}</version>
        </dependency>
        <dependency>
            <groupId>tech.codingzen</groupId>
            <artifactId>either</artifactId>
            <version>2.0.0</version>
        </dependency>

        <dependency>
            <groupId>com.willowtreeapps.assertk</groupId>
            <artifactId>assertk-jvm</artifactId>
            <version>0.23.1</version>
        </dependency>
        <dependency>
            <groupId>org.jetbrains.kotlin</groupId>
            <artifactId>kotlin-test-junit</artifactId>
            <version>${kotlin.version}</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>${junit.version}</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <sourceDirectory>src/main/kotlin</sourceDirectory>
        <testSourceDirectory>src/test/kotlin</testSourceDirectory>

        <plugins>
            <plugin>
                <groupId>org.jetbrains.kotlin</groupId>
                <artifactId>kotlin-maven-plugin</artifactId>
                <version>${kotlin.version}</version>
                <executions>
                    <execution>
                        <id>compile</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>compile</goal>
                        </goals>
                    </execution>
                    <execution>
                        <id>test-compile</id>
                        <phase>test-compile</phase>
                        <goals>
                            <goal>test-compile</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>3.2.1</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar-no-fork</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <!-- requires jdk8 -->
                    <plugin>
                        <groupId>org.jetbrains.dokka</groupId>
                        <artifactId>dokka-maven-plugin</artifactId>
                        <version>0.10.1</version>
                        <executions>
                            <execution>
                                <phase>prepare-package</phase>
                                <goals>
                                    <goal>dokka</goal>
                                    <goal>javadoc</goal>
                                    <goal>javadocJar</goal>
                                </goals>
                            </execution>
                        </executions>
                        <configuration>
                            <outputFormat>javadoc</outputFormat>
                            <outputDir>${project.build.directory}/javadoc</outputDir>
                        </configuration>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>1.6</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.8</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>true</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

</project>
