package tech.codingzen

import assertk.assertThat
import assertk.fail
import org.junit.Test

class EitherAssertkTests {
  @Test
  fun isLeft() {
    assertThat(Either.left(1)).isLeft()
    try {
      assertThat(Either.right(2)).isLeft()
      fail("isLeft should fail on right")
    } catch (e: AssertionError) {
      // test succeeded
    }
  }

  @Test
  fun isLeft_withValue() {
    assertThat(Either.left(1)).isLeft(1)
    try {
      assertThat(Either.left(2)).isLeft(3)
      fail("isLeft should fail when expected value does not match actual")
    } catch (e: AssertionError) {
      // test succeeded
    }
    try {
      assertThat(Either.right(2)).isLeft(2)
      fail("isLeft should fail on right")
    } catch (e: AssertionError) {
      // test succeeded
    }
  }

  @Test
  fun isRight() {
    assertThat(Either.right(1)).isRight()
    try {
      assertThat(Either.left(2)).isRight()
      fail("isRight should fail on left")
    } catch (e: AssertionError) {
      // test succeeded
    }
  }

  @Test
  fun isRight_withValue() {
    assertThat(Either.right(1)).isRight(1)
    try {
      assertThat(Either.right(2)).isRight(3)
      fail("isRight should fail when expected value does not match actual")
    } catch (e: AssertionError) {
      // test succeeded
    }
    try {
      assertThat(Either.right(2)).isLeft(2)
      fail("isRight should fail on left")
    } catch (e: AssertionError) {
      // test succeeded
    }
  }
}
